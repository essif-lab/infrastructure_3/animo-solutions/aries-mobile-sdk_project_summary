# Project Summary Aries Mobile SDK

**Are you interested in the Aries Mobile SDK or do you want to give your input on features/functionality? Please contact ana@animo.id.**

The Aries Mobile SDK is a complete toolkit for cross-platform iOS and Android development using React Native. With this one SDK, developers can build mobile SSI solutions for any use case. The SDK contains reusable issuer, holder and verifier components as well as essential components for mobile development (e.g. deeplinking, state management, biometric wallet unlocking). Developers can either start from scratch or integrate a selection of components into their existing mobile app. The Aries Mobile SDK is credential format and DID method agnostic, it uses the latest open standards to break out of the Indy & Aries ecosystem (e.g. W3C VCs, DIF PE, BBS+ Signatures).

The Aries Mobile SDK is 100% open-source, focuses on interoperability and open standards and is accessible for developers of any skill level. It aims to drastically lower the barrier of mobile SSI development.

## Technology description

The Aries Mobile SDK is built on top of Aries Framework JavaScript. Modules combine all recurring flows into high-level methods, allowing to e.g. create a connection and issue a revocable credential in a few lines of code.

The SDK uses DIDComm for secure and encrypted communication between agents. Aries protocols (AIP2.0) are used for creating connections and exchanging credentials (Indy, JSON-LD) and presentations (Indy, DIF PE). BBS+ signatures are used for W3C compliant privacy preserving VCs. Aries Askar provides encrypted storage as well as cryptographic methods used for encrypting DIDComm messages (V2 ready) and signing credentials (e.g. ed25519, bbs, p256, secp256k1). Interoperable mediation standards allow any agent to be used as a cloud-based connection point for mobile devices (e.g. ACA-Py, AF-GO). New React Native Codegen and Turbomodules are utilized to write a cross-platform wrapper around native rust libraries (Aries Askar, Indy Credx, Indy VDR) ([design](https://github.com/hyperledger/indy-vdr/issues/59)).

## Animo Solutions

The Aries Mobile SDK project is executed by Animo. Animo offers self-sovereign identity development and consultancy services to anyone working towards increasing digital autonomy and the adoption of self-sovereign identity solutions.

## Roadmap

![](./roadmap.png) 


